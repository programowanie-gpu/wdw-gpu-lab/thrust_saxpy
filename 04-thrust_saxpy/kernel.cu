
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <thrust/transform.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/functional.h>
#include <iostream>
#include <iterator>
#include <algorithm>

// This example illustrates how to implement the SAXPY
// operation (Y[i] = a * X[i] + Y[i]) using Thrust. 
// The saxpy_slow function demonstrates the most
// straightforward implementation using a temporary
// array and two separate transformations, one with
// multiplies and one with plus.  The saxpy_fast function
// implements the operation with a single transformation
// and represents "best practice".

// This example demonstrates the use of placeholders to implement
// the SAXPY operation (i.e. Y[i] = a * X[i] + Y[i]).
//
// Placeholders enable developers to write concise inline expressions
// instead of full functors for many simple operations.  For example,
// the placeholder expression "_1 + _2" means to add the first argument,
// represented by _1, to the second argument, represented by _2.
// The names _1, _2, _3, _4 ... _10 represent the first ten arguments
// to the function.
// 
// In this example, the placeholder expression "a * _1 + _2" is used
// to implement the SAXPY operation.  Note that the placeholder 
// implementation is considerably shorter and written inline. 


// allows us to use "_1" instead of "thrust::placeholders::_1"
using namespace thrust::placeholders;

struct saxpy_functor : public thrust::binary_function<float, float, float>
{
	const float a;

	saxpy_functor(float _a) : a(_a) {}

	__host__ __device__
		float operator()(const float& x, const float& y) const {
		return a * x + y;
	}
};

void saxpy_fast(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
	// Y <- A * X + Y
	thrust::transform(X.begin(), X.end(), Y.begin(), Y.begin(), saxpy_functor(A));
}

void saxpy_slow(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
	thrust::device_vector<float> temp(X.size());

	// temp <- A
	thrust::fill(temp.begin(), temp.end(), A);

	// temp <- A * X
	thrust::transform(X.begin(), X.end(), temp.begin(), temp.begin(), thrust::multiplies<float>());

	// Y <- A * X + Y
	thrust::transform(temp.begin(), temp.end(), Y.begin(), Y.begin(), thrust::plus<float>());
}

void saxpy_placeholders(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
	// Y <- A * X + Y
	thrust::transform(X.begin(), X.end(),  // input range #1
					  Y.begin(),           // input range #2
					  Y.begin(),           // output range
					  A * _1 + _2);        // placeholder expression
}

#ifndef __CUDACC_EXTENDED_LAMBDA__
#warning "please compile with --expt-extended-lambda"
#else
void saxpy_lambda(float A, thrust::device_vector<float>& X, thrust::device_vector<float>& Y)
{
	// Y <- A * X + Y
	thrust::transform(X.begin(), X.end(),  // input range #1
					  Y.begin(),           // input range #2
					  Y.begin(),           // output range
					  [=] __host__ __device__(float x, float y) { return A * x + y; });   // lambda expression, --expt-extended-lambda
}
#endif


int main(void)
{
	// initialize host arrays
	float x[4] = { 1.0, 1.0, 1.0, 1.0 };
	float y[4] = { 1.0, 2.0, 3.0, 4.0 };

	{
		// transfer to device
		thrust::device_vector<float> X(x, x + 4);
		thrust::device_vector<float> Y(y, y + 4);

		// slow method
		saxpy_slow(2.0, X, Y);
		std::cout << "saxpy_slow finished\n";
	}

	{
		// transfer to device
		thrust::device_vector<float> X(x, x + 4);
		thrust::device_vector<float> Y(y, y + 4);

		// fast method
		saxpy_fast(2.0, X, Y);
		std::cout << "saxpy_fast finished\n";
	}

	{
		// transfer to device
		thrust::device_vector<float> X(x, x + 4);
		thrust::device_vector<float> Y(y, y + 4);

		// using placeholders
		// https://github.com/thrust/thrust/blob/master/examples/lambda.cu
		saxpy_placeholders(2.0, X, Y);
		std::cout << "saxpy_placeholders finished\n";
	}

#ifdef __CUDACC_EXTENDED_LAMBDA__
	{
		// transfer to device
		thrust::device_vector<float> X(x, x + 4);
		thrust::device_vector<float> Y(y, y + 4);

		// using lambda expressions, require --expt-extended-lambda
		// https://stackoverflow.com/questions/30438538/lambda-expressions-with-cuda
		saxpy_lambda(2.0, X, Y);
		std::cout << "saxpy_lambda finished\n";
	}
#endif

	return 0;
}


